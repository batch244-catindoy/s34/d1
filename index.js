// this is used to get the contents of the express package to be used by the application
const express = require("express");

// Creates an application using express
// This creates an express application and stores it in a variable
// In layman's term, app is our server
const app = express();

// for our application server to run, we need a port to listen to
const port = 3000;

// Setup for allowing the server to handle data from requests
// Allows our app to read JSON data
// Middleware is a request handler that has access to the application request and response cycle
app.use(express.json())

// Mock database
let users = [
	{
		username: "TStark3000",
		email: "starkindustries@mail.com",
		password: "notPeterParker"
	},
	{
		username: "ThorThunder",
		email: "loveAndThunder@mail.com",
		password: "iLoveStormBreaker"
	}
];

// [SECTION] Routes

// Express has methods corresponding to each http method

// This routee espects to receive a GET request at the base URI
//This will return a simple message back to client
app.get("/", (req, res) => {
	// Combines writeHead() and end()
	// it used the express JS module's method instead to send a response back to the client
	res.send("Avengers Assemble")
});

app.get("/greeting", (req, res) => {
	
	res.send("Hello from Batch244-Hulk")
});

// This route expects to receive a POST request

app.post("/hello", (req, res) =>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
});

// This route expects to recieve a post request at the URI "/signup"
app.post("/signup", (req, res) => {
	if(req.body.username !== "" && req.body.password !== "" && req.body.email !== ""){
		users.push(req.body)
		res.send(`User ${req.body.username} is successfully registered!`)
	}
	else {
		res.send("Please input BOTH usename and password")
	}
});
app.get("/list", (req, res) => {
	
	res.send(users)
});

// This route expects to receive a PUT request
// This will update the password of user that matches the information provided in the Client/Postman
app.put("/change-password", (req, res) => {
	let message;

	for(let i = 0; i < users.length; i++){

		if (req.body.username == users[i].username){
			users[i].password = req.body.password

			message = `User ${req.body.username}'s password has been updated`
			break;
		} else {
			message = "User does not exist."
		}	
	}
	res.send(message);
})

app.delete("/delete-user", (req, res) => {
	if(req.username == req.username){
		for(let i = 0; i < users.length; i++){
			users.splice(i, 1)
			res.send(`User ${req.body.username} has been deleted.`)
		}
	}
	else {
		res.send("")
	}
});

app.get("/home", (req, res) => {
	
	res.send("Welcome to the home page")
});
	
	







app.listen(port, () => console.log(`Server is running at port ${port}`))